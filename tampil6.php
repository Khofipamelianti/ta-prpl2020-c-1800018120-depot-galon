
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Data Penyimpanan Barang</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<div class="pos-f-t">
  <div class="collapse" id="navbarToggleExternalContent">
    <div class="bg-dark p-4">
      <h4 class="text-white"><a href="index.html"> <img src="img/home2.png" width=40px height=40px ></a></h4>
      <span class="text-muted">home</span>
    </div>
  </div>
  <nav class="navbar navbar-dark bg-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  </nav>
</div>	

<div id="wrapper">
	<div id="header">
<div class="container p-3">

  <h2>PENYIMPANAN BARANG/PRODUK YANG TELAH TERJUAL </h2>
  <p>Data beli</p>     
  <?php 

include "connect.php";

$data = mysqli_query($connect,"SELECT * FROM pembelian join barang on barang.idbarang=pembelian.idbarang");

?>       
  <table class="table table-dark table-striped">
    <thead>
      <tr>
	<th>Id pembelian</th>
	<th>Id pelanggan</th>
	<th>Id barang</th>
  <th>Data Barang Terjual</th>
	<th>Jumlah beli</th>
  <th>harga barang</th>
  <th>Total Bayar</th>
  <th>Keterangan</th>
	</tr>
    </thead>
    <tbody>
      <?php 
	foreach ($data as $x) {
		?>
		<tr>
			<td><?= $x['idpembelian']; ?></td>
			<td><?= $x['idpelanggan']; ?></td>
			<td><?= $x['idbarang']; ?></td>
			<td><?= $x['jenisbarang']; ?></td>

      <td><?= $x['jumlahbeli']; ?></td>
      <td><?= $x['hargabarang'];?></td>
      <td><?= $x['hargabarang']*$x['jumlahbeli']; ?></td>
      <td>Lunas</td>
		<td><a href="hapus4.php?idpembelian=<?php echo $x['idpembelian']; ?>">hapus</a>
		<a href="edit4.php?idpembelian=<?php echo $x['idpembelian']; ?>">edit</a>
    <a href="cetak4.php?idpembelian=<?php echo $x['idpembelian']; ?>">cetak</a></td>

		</tr>
		<tr>
			
		</tr>
		<?php
  

	};
	?>
  <nav class="navbar navbar-light bg-light">
  <form class="form-inline">
    <button class="btn btn-outline-success" type="button"><a href="tambah4.php?idpembelian=<?php echo $x['idpembelian']; ?>">Klik disini untuk Membeli</a></button>
  </form>
</nav>

</table>
    </tbody>
  </table>
</div>
</div>
</div>
</body>
</html>
